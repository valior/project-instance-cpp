#include <cstdlib>
#include <cstdio>
#include <iostream>

#include "arithmetic.h"

using namespace std;

int main()
{
    cout << "Hello C++ arithmetic application" << endl;

    cout << "Input two numbers:" << endl;

    double a = 0;
    double b = 0;

    cin >> a;
    cin >> b;

    cout << "Result of multiply is " << multiply(a, b) << endl;

    int size_of_ptr = sizeof(int*);
    printf("Pointer size is %i \n", size_of_ptr);

    return EXIT_SUCCESS;
}
