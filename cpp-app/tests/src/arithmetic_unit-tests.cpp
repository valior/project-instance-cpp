#include <gtest/gtest.h>
#include "arithmetic.h"


TEST(Arithmetic, Mul_Success)
{
    EXPECT_EQ(4, multiply(2, 2));
    EXPECT_EQ(100, multiply(10, 10));
    EXPECT_EQ(48, multiply(6, 8));
}

TEST(Arithmetic, Div_Success)
{
    double expectedResult = 0.0;
    double actualResult = 0.0;

    expectedResult = 2;
    actualResult = divide(4, 2);
    EXPECT_FLOAT_EQ(expectedResult, actualResult);

    expectedResult = 10;
    actualResult = divide(100, 10);
    EXPECT_FLOAT_EQ(expectedResult, actualResult);

    expectedResult = 2.5;
    actualResult = divide(5, 2);
    EXPECT_FLOAT_EQ(expectedResult, actualResult);
}
