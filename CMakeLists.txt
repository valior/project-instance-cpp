project(project-instance-cpp NONE)
cmake_minimum_required(VERSION 2.8.12)

## Global target for build all targets for distribution needs (without tests)
add_custom_target(dist)

## Global targets for build and run all tests targets
add_custom_target(tests)
add_custom_target(check)
add_custom_target(memcheck)

## Global targets for static analyzer commands
add_custom_target(cppcheck)
add_custom_target(vera)

## C++ Test Framework
add_subdirectory(thirdparty/googletest)

## C Application Instance
add_subdirectory(c-app)

## C++ Application Instance
add_subdirectory(cpp-app)
