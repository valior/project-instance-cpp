extern "C"
{
#include "arithmetic.h"
}

#include <gtest/gtest.h>


TEST(Arithmetic, Sum_Success)
{
    EXPECT_EQ(4, sum(2, 2));
    EXPECT_EQ(10, sum(5, 5));
    EXPECT_EQ(24, sum(11, 13));
}

TEST(Arithmetic, Sub_Success)
{
    double expectedResult = 0.0;
    double actualResult = 0.0;

    expectedResult = 2;
    actualResult = sub(4, 2);
    EXPECT_FLOAT_EQ(expectedResult, actualResult);

    expectedResult = 5;
    actualResult = sub(10, 5);
    EXPECT_FLOAT_EQ(expectedResult, actualResult);

    expectedResult = 25;
    actualResult = sub(40, 15);
    EXPECT_FLOAT_EQ(expectedResult, actualResult);
}
